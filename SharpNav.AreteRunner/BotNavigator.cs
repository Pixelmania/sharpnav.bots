﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.Inventory;
using AOSharp.Core.UI;
using AOSharp.Pathfinding;
using SharpNav;
using SmokeLounge.AOtomation.Messaging.GameData;
using SmokeLounge.AOtomation.Messaging.Messages;
using SmokeLounge.AOtomation.Messaging.Messages.N3Messages;
using System;

namespace AreteRunner
{
    public class BotNavigator
    {
        public static void Initialize()
        {
            if (Playfield.ModelIdentity.Instance != Const.ARETE_PF)
            {
                Chat.WriteLine("You must be in arete to start this bot!");
                return;
            }

            string navPath = $"{Globals.PLUGIN_DIR}\\{Const.ARETE_PF}.nav";

            if (!SNavMeshSerializer.LoadFromFile(navPath, out NavMesh navMesh))
            {
                Chat.WriteLine($"Could not find navmesh file at given path! {navPath}");
                return;
            }

            SMovementController.Set();
            SMovementController.LoadNavmesh(navMesh);

            BotAction.Init();
            Game.TeleportEnded += OnTeleportEnded;
            DynelManager.DynelSpawned += OnDynelSpawned;
            Network.N3MessageReceived += OnN3MessageReceived;
            SMovementController.DestinationReached += OnDestinationReached;
        }

        private static void OnDynelSpawned(object sender, Dynel e)
        {
            if (e.Name != Dynels.CREDIT_CARD || Inventory.Find(Const.CREDIT_CARD_ITEM, out _) || Utils.UsedCreditCard())
                return;

            BotAction.UseDynel(Dynels.CREDIT_CARD);
        }

        private static void OnN3MessageReceived(object sender, N3Message n3Msg)
        {
            if (n3Msg.Identity != DynelManager.LocalPlayer.Identity)
                return;

            switch (n3Msg.N3MessageType)
            {
                case N3MessageType.ContainerAddItem:
                    Utils.UseLastItem();
                    break;
                case N3MessageType.CharacterAction:
                    CharacterActionMessage charMsg = (CharacterActionMessage)n3Msg;
                    if (charMsg.Action == CharacterActionType.DeleteItem)
                        SMovementController.SetNavDestination(Location.GATE);
                    break;
                case N3MessageType.KnubotOpenChatWindow:
                    BotAction.GiveCreditsToNpc();
                    break;
                case N3MessageType.Quest:
                    BotAction.UseDynel(Dynels.PORTAL_TERMINAL);
                    break;
            }
        }

        private static void OnDestinationReached(Vector3 destination)
        {
            Vector3 checkpoint = BotAction.GetCheckpoint();

            BotAction.UseDynel(checkpoint == Location.CREDIT_CARD ? Dynels.CREDIT_CARD : checkpoint == Location.GATE ?
                DynelManager.LocalPlayer.GetStat(Stat.Cash) == Const.POST_QUEST_CASH ?
                Dynels.PORTAL_TERMINAL : Dynels.QUEST_NPC : "");

            Utils.UpdatePosition();
        }

        private static void OnTeleportEnded(object sender, EventArgs e)
        {
            if (Playfield.ModelIdentity.Instance != Const.ARETE_PF)
                return;

            SMovementController.Halt();
            BotAction.Init();
        }
    }
}