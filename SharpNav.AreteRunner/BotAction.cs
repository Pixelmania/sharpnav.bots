﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.Inventory;
using AOSharp.Pathfinding;
using System.Linq;

namespace AreteRunner
{
    public class BotAction
    {
        internal static void Init()
        {
            Globals.NEXT_FREE_INVENTORY_SLOT = Utils.GetNextAvailableSlot();

            if (Inventory.Find(Const.CREDIT_CARD_ITEM, out Item creditCard))
                creditCard.Use();
            else if (Utils.UsedCreditCard())
                SMovementController.SetNavDestination(Location.GATE);
            else
                SMovementController.SetNavDestination(Location.CREDIT_CARD);
        }

        internal static void UseDynel(string dynelName)
        {
            if (!DynelManager.Find(dynelName, out Dynel selectedDynel))
                return;

            Targeting.SetTarget(selectedDynel.Identity);
            Utils.UseDynel();
        }

        internal static void GiveCreditsToNpc()
        {            
            Utils.KnubotStartTrade();
            Utils.KnubotFinishTrade();
        }

        internal static Vector3 GetCheckpoint() => new Vector3[2] { Location.CREDIT_CARD, Location.GATE }
                .OrderBy(x => Vector3.Distance(DynelManager.LocalPlayer.Position, x))
                .First();
    }
}