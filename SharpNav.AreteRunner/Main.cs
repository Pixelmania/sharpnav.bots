﻿using AOSharp.Core;
using AOSharp.Core.UI;

namespace AreteRunner
{
    public class Main : AOPluginEntry
    {

        public override void Run(string pluginDir)
        {
            Chat.WriteLine("Arete Runner");
            Chat.WriteLine("/areterunner - starts bot");

            Globals.PLUGIN_DIR = pluginDir;

            Chat.RegisterCommand("areterunner", (string command, string[] param, ChatWindow chatWindow) =>
            {
                BotNavigator.Initialize();
            });

        }
    }
}